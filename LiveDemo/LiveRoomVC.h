//
//  LiveRoomVC.h
//  LiveDemo
//
//  Created by Eric on 2017/6/28.
//  Copyright © 2017年 YangWeiCong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

/**
 直播间状态

 - LiveRoomTypeAnchor: 主播
 - LiveRoomTypeAudience: 观众
 */
typedef NS_ENUM(NSInteger,LiveRoomType) {
    LiveRoomTypeAnchor,
    LiveRoomTypeAudience,
};


@interface LiveRoomVC : UIViewController

@property (nonatomic, assign) LiveRoomType roomType;

@end
